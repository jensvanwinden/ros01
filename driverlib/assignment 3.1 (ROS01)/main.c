#include <stdint.h>
#include <stddef.h>
#include "register_def.h"

#include <gpio.h>
#include <pin.h>
#include <prcm.h>
#include <systick.h>

enum colors
{
    red, orange, green
};

enum colors color = red;

void IntISR();
int setTrafficLight(int time);
void trafficLight(enum colors led);

int wait_1_sec = 0;

#define ledRed GPIO_PIN_1
#define ledOrange GPIO_PIN_2
#define ledGreen GPIO_PIN_3

int main(void)
{
    // set pad configs to 0x20
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_9) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_10) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_11) = 0x20;

    // enable GPIO1CLKEN
    //PRCMPeripheralClkEnable(APPS_RCM_O_GPIO_B_CLK_GATING, PRCM_RUN_MODE_CLK);

    // enable 3 LED's as output
    PinTypeGPIO(ledRed | ledOrange | ledGreen, PIN_MODE_0, false);

    // set io pins to be outputs (base, select your pins, in/output mode for them)
    GPIODirModeSet(GPIOA1_BASE, ledRed | ledOrange | ledGreen,
    GPIO_DIR_MODE_OUT);

    //  write io pins high. (base, mask for pins you want to write, pins to be high)
    GPIOPinWrite(GPIOA1_BASE, ledRed | ledOrange | ledGreen,
    ledRed | ledOrange | ledGreen);

    //enable sysTick
    SysTickEnable();

    //register the ISR
    SysTickIntRegister(&IntISR);

    //counter period, 80k bc: 80Mhz = clk, ISR counts up to 1k, together its 80M
    SysTickPeriodSet(80000);

    while (1)
    {
        //__asm ("     WFI");   //assignment 2.4
        PRCMSleepEnter();       //assignment 2.5
        wait_1_sec = setTrafficLight(wait_1_sec);
    }
}

void IntISR()
{
    static unsigned int i = 0;
    i++;

    if (i >= 1000)
    {
        wait_1_sec++;
        i = 0;
    }
}

int setTrafficLight(int time)
{
    if (time == 0)
    {
        trafficLight(red);
    }

    else if (time == 5)
    {
        trafficLight(orange);
    }

    else if (time > 5 && wait_1_sec <= 8)
    {
        trafficLight(green);
    }

    else if (time == 10)
    {
        time = 0;
    }

    return time;
}

void trafficLight(enum colors led)
{
    switch (led)
    {
    case red:
        GPIOPinWrite(GPIOA1_BASE, ledRed | ledOrange | ledGreen,
        ledRed);
        break;

    case orange:
        GPIOPinWrite(GPIOA1_BASE, ledRed | ledOrange | ledGreen,
        ledOrange);
        break;

    case green:
        GPIOPinWrite(GPIOA1_BASE, ledRed | ledOrange | ledGreen,
        ledGreen);
        break;

    default:
        break;
    }
}

