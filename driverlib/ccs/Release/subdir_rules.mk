################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
adc.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/adc.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

aes.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/aes.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

camera.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/camera.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

cpu.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/cpu.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

crc.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/crc.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

des.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/des.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

flash.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/flash.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

gpio.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/gpio.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

hwspinlock.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/hwspinlock.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

i2c.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/i2c.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

i2s.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/i2s.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

interrupt.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/interrupt.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

pin.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/pin.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

prcm.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/prcm.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

sdhost.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/sdhost.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

shamd5.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/shamd5.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

spi.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/spi.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

systick.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/systick.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

timer.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/timer.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

uart.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/uart.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

udma.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/udma.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

utils.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/utils.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

wdt.obj: C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/wdt.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib --abi=eabi -me -Ooff --include_path="C:/ti/ccs910/ccs/tools/compiler/ti-cgt-arm_18.12.2.LTS/include" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/inc" --include_path="C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/" -g --gcc --define=ccs --define=DRIVERLIB_APPS --define=cc3200 --diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --printf_support=full --ual --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


