################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/adc.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/aes.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/camera.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/cpu.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/crc.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/des.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/flash.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/gpio.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/hwspinlock.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/i2c.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/i2s.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/interrupt.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/pin.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/prcm.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/sdhost.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/shamd5.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/spi.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/systick.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/timer.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/uart.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/udma.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/utils.c \
C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/wdt.c 

C_DEPS += \
./adc.d \
./aes.d \
./camera.d \
./cpu.d \
./crc.d \
./des.d \
./flash.d \
./gpio.d \
./hwspinlock.d \
./i2c.d \
./i2s.d \
./interrupt.d \
./pin.d \
./prcm.d \
./sdhost.d \
./shamd5.d \
./spi.d \
./systick.d \
./timer.d \
./uart.d \
./udma.d \
./utils.d \
./wdt.d 

OBJS += \
./adc.obj \
./aes.obj \
./camera.obj \
./cpu.obj \
./crc.obj \
./des.obj \
./flash.obj \
./gpio.obj \
./hwspinlock.obj \
./i2c.obj \
./i2s.obj \
./interrupt.obj \
./pin.obj \
./prcm.obj \
./sdhost.obj \
./shamd5.obj \
./spi.obj \
./systick.obj \
./timer.obj \
./uart.obj \
./udma.obj \
./utils.obj \
./wdt.obj 

OBJS__QUOTED += \
"adc.obj" \
"aes.obj" \
"camera.obj" \
"cpu.obj" \
"crc.obj" \
"des.obj" \
"flash.obj" \
"gpio.obj" \
"hwspinlock.obj" \
"i2c.obj" \
"i2s.obj" \
"interrupt.obj" \
"pin.obj" \
"prcm.obj" \
"sdhost.obj" \
"shamd5.obj" \
"spi.obj" \
"systick.obj" \
"timer.obj" \
"uart.obj" \
"udma.obj" \
"utils.obj" \
"wdt.obj" 

C_DEPS__QUOTED += \
"adc.d" \
"aes.d" \
"camera.d" \
"cpu.d" \
"crc.d" \
"des.d" \
"flash.d" \
"gpio.d" \
"hwspinlock.d" \
"i2c.d" \
"i2s.d" \
"interrupt.d" \
"pin.d" \
"prcm.d" \
"sdhost.d" \
"shamd5.d" \
"spi.d" \
"systick.d" \
"timer.d" \
"uart.d" \
"udma.d" \
"utils.d" \
"wdt.d" 

C_SRCS__QUOTED += \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/adc.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/aes.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/camera.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/cpu.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/crc.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/des.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/flash.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/gpio.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/hwspinlock.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/i2c.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/i2s.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/interrupt.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/pin.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/prcm.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/sdhost.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/shamd5.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/spi.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/systick.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/timer.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/uart.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/udma.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/utils.c" \
"C:/ti/simplelink_cc32xx_sdk_3_20_00_06/source/ti/devices/cc32xx/driverlib/wdt.c" 


