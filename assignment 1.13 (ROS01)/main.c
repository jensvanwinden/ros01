#include <stdint.h>
#include <stddef.h>
#include "register_def.h"

#include <gpio.h>
#include <pin.h>
#include <prcm.h>

//Extra includes hier onder

int main(void)
{
    // set pad configs to 0x20
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_9) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_10) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_11) = 0x20;

    // enable GPIO1CLKEN
    PRCMPeripheralClkEnable(APPS_RCM_O_GPIO_B_CLK_GATING, PRCM_RUN_MODE_CLK);

    // enable 3 LED's as output
    PinTypeGPIO(GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, PIN_MODE_0, false);

    // set io pins to be outputs (base, select your pins, in/output mode for them)
    GPIODirModeSet(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_DIR_MODE_OUT);

    //  write io pins high. (base, mask for pins you want to write, pins to be high)
    GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    int value = 0;
    while (1)
    {
        if (value >= 16)
        {
            value = 0;
        }
        for (int i = 0; i < 6300000; i++) //wait for prox 1 sec
        {

        }

        // write LED's according to table given in 1.10
        GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, value);
        value = value + 2;
    }
}
