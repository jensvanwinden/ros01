#include <stdint.h>
#include <stddef.h>
#include "register_def.h"
//Extra includes hier onder

int main(void)
{
    // set pad configs to 0x20
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_9) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_10) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_11) = 0x20;

    // enable GPIO1CLKEN
    HWREG(ARCM_BASE + APPS_RCM_O_GPIO_B_CLK_GATING + (0x1>>2)) = APPS_RCM_GPIO_B_CLK_GATING_GPIO_B_RUN_CLK_ENABLE;

    // enable 3 LED's as output
    HWREG(GPIOA1_BASE + GPIO_O_GPIO_DIR) = 0xE;

    // write 3 LED's off
    HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0xE<<2)) = 0;

    int value = 0;
    while (1)
    {
        if (value >= 16)
        {
            value = 0;
        }
        for (int i = 0; i < 6300000; i++) //wait for prox 1 sec
        {

        }

        // write LED's according to table given in 1.10
        HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0xE<<2)) = value;
        value = value + 2;
    }
}
