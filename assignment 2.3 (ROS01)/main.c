#include <stdint.h>
#include <stddef.h>
#include "register_def.h"

#include <gpio.h>
#include <pin.h>
#include <prcm.h>
#include <systick.h>

void IntISR();
int wait_1_sec = 0;

int main(void)
{
    // set pad configs to 0x20
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_9) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_10) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_11) = 0x20;

    // enable GPIO1CLKEN
    // PRCMPeripheralClkEnable(APPS_RCM_O_GPIO_B_CLK_GATING, PRCM_RUN_MODE_CLK);
    // enable 3 LED's as output
    PinTypeGPIO(GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, PIN_MODE_0, false);
    // set io pins to be outputs (base, select your pins, in/output mode for them)
    GPIODirModeSet(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3,
                   GPIO_DIR_MODE_OUT);
    //  write io pins high. (base, mask for pins you want to write, pins to be high)
    GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3,
                 GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    //enable sysTick
    SysTickEnable();
    //register the ISR
    SysTickIntRegister(&IntISR);
    //counter period, 80k bc: 80Mhz = clk, ISR counts up to 1k, together its 80M
    SysTickPeriodSet(80000);

    while (1)
    {
        //__asm ("     WFI");   //assignment 2.4
        PRCMSleepEnter();       //assignment 2.5

        if (wait_1_sec == 0)
        {
            GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3,
                         GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
        }
        else
        {
            GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3,
                         GPIO_PIN_3);
        }
    }
}

void IntISR()
{
    static unsigned int i = 0;
    i++;

    if (i >= 1000)
    {
        wait_1_sec ^= 1;
        i = 0;
    }
}
