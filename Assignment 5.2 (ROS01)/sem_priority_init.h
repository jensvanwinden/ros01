// Implementation of sem_init which uses a priority based waiting queue

int sem_priority_init(sem_t *semaphore, int pshared, unsigned value);
