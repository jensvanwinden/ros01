#include <stdint.h>
#include <stddef.h>
#include "register_def.h"
//Extra includes hier onder

#define RELOAD_1ms 80000

volatile int wait_1_sec = 0;


int main(void)
{
    //Hier komt jouw code
    HWREG(NVIC_ST_RELOAD) = RELOAD_1ms;             //set the reload top value to 1ms/80000 cycles, making 1khz
    HWREG(NVIC_ST_CURRENT) = RELOAD_1ms;            //init the timer on the top value

    HWREG(NVIC_ST_CTRL) |= NVIC_ST_CTRL_CLK_SRC | NVIC_ST_CTRL_ENABLE;
    HWREG(NVIC_ST_CTRL) |= NVIC_ST_CTRL_INTEN;

    // set pad configs to 0x20
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_9) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_10) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_11) = 0x20;

    // enable GPIO1CLKEN
    HWREG(ARCM_BASE + APPS_RCM_O_GPIO_B_CLK_GATING + (0x1>>2)) = APPS_RCM_GPIO_B_CLK_GATING_GPIO_B_RUN_CLK_ENABLE;

    // enable 3 LED's as output
    HWREG(GPIOA1_BASE + GPIO_O_GPIO_DIR) = 0xE;

    // write 3 LED's off
    HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0xE<<2)) = 0;


    while(1)
    {
        if (wait_1_sec == 0)
        {
            HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0xE<<2)) = 4;
        }
        else {HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0xE<<2)) = 0;}
    }
}
