#include <stdint.h>
#include <stddef.h>
#include "register_def.h"

#include <gpio.h>
#include <pin.h>
#include <prcm.h>
#include <systick.h>

enum colors{red, orange, green};
enum colors color = red;
enum taskState{READY, WAITING};

void IntISR();
void initGPIO(void);
void turnOnRedLed(void);
void turnOnGreenLed(void);
void turnOnOrangeLed(void);
void runReadyTasks();
void addTaskToList(void(*function)(void), uint16_t interval, uint16_t delay);

#define ledRed GPIO_PIN_1
#define ledOrange GPIO_PIN_2
#define ledGreen GPIO_PIN_3
#define allLeds (ledRed | ledOrange | ledGreen)

#define MAX_TASKS 8

#define INTERVAL_10MS 10
#define INTERVAL_50MS 50
#define INTERVAL_100MS 100
#define INTERVAL_200MS 200
#define INTERVAL_300MS 300
#define INTERVAL_500MS 500
#define INTERVAL_750MS 750

typedef struct
{
    uint16_t interval;
    int16_t counter;
    uint16_t initDelay;
    void (*func)(void);
    enum taskState state;
} taskType;

taskType tasks[MAX_TASKS];

int main(void)
{
    initGPIO();

    //enable sysTick
    SysTickEnable();
    //register the ISR
    SysTickIntRegister(&IntISR);
    //counter period, 80k bc: 80Mhz = clk, ISR counts up to 1k, together its 80M
    SysTickPeriodSet(80000 - 1);

    addTaskToList(turnOnRedLed, INTERVAL_200MS, INTERVAL_100MS);
    addTaskToList(turnOnGreenLed, INTERVAL_500MS, INTERVAL_200MS);
    addTaskToList(turnOnOrangeLed, INTERVAL_750MS, INTERVAL_300MS);

    while (1)
    {
        PRCMSleepEnter();
        runReadyTasks();
    }
}

void addTaskToList(void(*function)(void), uint16_t interval, uint16_t delay)
{
    taskType newTask;

    newTask.func = function;
    newTask.interval = interval;
    newTask.counter = -delay;
    newTask.state = WAITING;
    newTask.initDelay = delay;

    for (volatile int i = 0; i < 8; i++)
    {
        if(tasks[i].func == 0)
        {
            tasks[i] = newTask;
            return;
        }
    }
    return;
}

void IntISR()
{
    for (volatile int i = 0; i < MAX_TASKS; i++)
    {
        tasks[i].counter++;

        if (tasks[i].counter >= tasks[i].interval)
        {
            tasks[i].state = READY;
        }
    }
}

void runReadyTasks()
{
    for (volatile int i = 0; i < MAX_TASKS; i++)
    {
        if(tasks[i].state == READY)
        {
            if (tasks[i].func == 0)
            {
                return;
            }

            tasks[i].state = WAITING;
            tasks[i].counter = 0;
            tasks[i].func();

        }
    }
}

void turnOnRedLed(void)
{
    HWREG(GPIOA1_BASE + (allLeds<<2)) ^= ledRed;
}

void turnOnGreenLed(void)
{
    HWREG(GPIOA1_BASE + (allLeds<<2)) ^= ledGreen;
}

void turnOnOrangeLed(void)
{
    HWREG(GPIOA1_BASE + (allLeds<<2)) ^= ledOrange;
}

void initGPIO(void)
{
    // set pad configs to 0x20
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_9) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_10) = 0x20;
    HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_11) = 0x20;

    // enable GPIO1CLKEN
    //PRCMPeripheralClkEnable(APPS_RCM_O_GPIO_B_CLK_GATING, PRCM_RUN_MODE_CLK);

    // enable 3 LED's as output
    PinTypeGPIO(allLeds, PIN_MODE_0, false);

    // set io pins to be outputs (base, select your pins, in/output mode for them)
    GPIODirModeSet(GPIOA1_BASE, allLeds,
    GPIO_DIR_MODE_OUT);

    //  write io pins high. (base, mask for pins you want to write, pins to be high)
    GPIOPinWrite(GPIOA1_BASE, allLeds, 0);
    HWREG(GPIOA1_BASE + (allLeds<<2)) ^= allLeds;
    HWREG(GPIOA1_BASE + (allLeds<<2)) ^= allLeds;
    HWREG(GPIOA1_BASE + (allLeds<<2)) ^= allLeds;
    HWREG(GPIOA1_BASE + (allLeds<<2)) ^= allLeds;
}
